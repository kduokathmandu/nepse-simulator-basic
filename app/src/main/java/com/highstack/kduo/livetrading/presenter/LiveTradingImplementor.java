package com.highstack.kduo.livetrading.presenter;

import android.content.Context;
import android.util.Log;


import com.highstack.kduo.api.ApiInterface;
import com.highstack.kduo.helper.NoInternetConnectionHelper;
import com.highstack.kduo.livetrading.model.LiveTradingParser;
import com.highstack.kduo.livetrading.view.LiveTradingView;
import com.highstack.kduo.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingImplementor implements LiveTradingPresenter {

    ArrayList<ArrayList<String>> tableContentsList;
    LiveTradingView liveTradingView;
    Context context;

    public LiveTradingImplementor(LiveTradingView liveTradingView, Context context) {
        this.liveTradingView = liveTradingView;
        this.context = context;
    }

    @Override
    public void getLiveTradingData() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getLiveTrading();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("totalGain", response.raw().request().url() + "  ");
                if(response.isSuccessful()) {
                    try {
                        LiveTradingParser liveTradingParser = new LiveTradingParser(response.body().string());
                        liveTradingParser.parse();
                        tableContentsList = liveTradingParser.getContentsWithValue();
//                        liveTradingView.setAdapter();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}

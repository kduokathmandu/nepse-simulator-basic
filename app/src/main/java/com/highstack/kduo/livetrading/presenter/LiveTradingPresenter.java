package com.highstack.kduo.livetrading.presenter;

/**
 * Created by Own on 9/2/2017.
 */

public interface LiveTradingPresenter {

    void getLiveTradingData();
}

package com.highstack.kduo.livetrading.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveTradingDataModel {

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("noOfTransactions")
    @Expose
    private Integer noOfTransactions;

    @SerializedName("maxPrice")
    @Expose
    private Integer maxPrice;

    @SerializedName("minPrice")
    @Expose
    private Integer minPrice;

    @SerializedName("closingPrice")
    @Expose
    private Integer closingPrice;

    @SerializedName("tradedShares")
    @Expose
    private Integer tradedShares;

    @SerializedName("amount")
    @Expose
    private Integer amount;

    @SerializedName("previousClosing")
    @Expose
    private Integer previousClosing;

    @SerializedName("difference")
    @Expose
    private Integer difference;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getNoOfTransactions() {
        return noOfTransactions;
    }

    public void setNoOfTransactions(Integer noOfTransactions) {
        this.noOfTransactions = noOfTransactions;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(Integer closingPrice) {
        this.closingPrice = closingPrice;
    }

    public Integer getTradedShares() {
        return tradedShares;
    }

    public void setTradedShares(Integer tradedShares) {
        this.tradedShares = tradedShares;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPreviousClosing() {
        return previousClosing;
    }

    public void setPreviousClosing(Integer previousClosing) {
        this.previousClosing = previousClosing;
    }

    public Integer getDifference() {
        return difference;
    }

    public void setDifference(Integer difference) {
        this.difference = difference;
    }
}

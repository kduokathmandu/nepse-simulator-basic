package com.highstack.kduo.livetrading.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.highstack.kduo.MainActivity;
import com.highstack.kduo.R;
import com.highstack.kduo.base.BaseFragment;
import com.highstack.kduo.livetrading.presenter.LiveTradingImplementor;
import com.highstack.kduo.livetrading.presenter.LiveTradingPresenter;
import com.highstack.kduo.utils.Constant;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingFragment extends BaseFragment implements LiveTradingView {


    @Override
    protected int getLayout() {
        return R.layout.live_trading;
    }

    @Override
    protected void init(View view) {
        LiveTradingPresenter liveTradingPresenter = new LiveTradingImplementor(this,getActivity());
        liveTradingPresenter.getLiveTradingData();

    }

    @Override
    public void onResume() {
        super.onResume();

    }



}

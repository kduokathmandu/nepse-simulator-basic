package com.highstack.kduo.utils;


import com.highstack.kduo.R;
import com.highstack.kduo.base.BaseActivity;

/**
 * Created by Own on 1/28/2018.
 */

public class NoInternetClass extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.no_internt;
    }

    @Override
    protected void init() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

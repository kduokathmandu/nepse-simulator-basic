package com.highstack.kduo.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.highstack.kduo.utils.NoInternetClass;


/**
 * Created by Own on 12/20/2017.
 */

public class NoInternetConnectionHelper {


    public static void showDialog(final Context context) {
       /* final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.no_internet_connection);
        d.setCanceledOnTouchOutside(false);
        d.setCancelable(false);
        TextView tt_dialog_msg = (TextView) d.findViewById(R.id.tt_dialog_msg);
        TextView no = (TextView) d.findViewById(R.id.tt_okay);

        no.setText(context.getString(R.string.okay));
        tt_dialog_msg.setText(context.getString(R.string.no_internet_connection));
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
                ((Activity) context).finish();
            }
        });

        d.show();*/

        Intent i = new Intent(context, NoInternetClass.class);
        context.startActivity(i);
        ((Activity) context).finish();
    }
}

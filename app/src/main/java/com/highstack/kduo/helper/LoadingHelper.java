package com.highstack.kduo.helper;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.ImageView;

import com.highstack.kduo.R;

/**
 * Created by Own on 7/11/2017.
 */

public class LoadingHelper {


    Dialog dialog;
    Context context;
    ImageView imageView;

    public LoadingHelper(@NonNull Context context) {
        this.context = context;
    }

    public void showDialog(){
        dialog = new Dialog(context,android.R.style.Theme_Black_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_dailog);
//        imageView = (ImageView)dialog.findViewById(R.id.iv_loading);
   /*     ObjectAnimator animatorRotation = ObjectAnimator.ofFloat(imageView1, "svgRotation", 0, 360);
        animatorRotation.setDuration(2000);
        animatorRotation.setRepeatCount(ValueAnimator.INFINITE);
        animatorRotation.setInterpolator(new LinearInterpolator());
        animatorRotation.start();*/
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent_cc);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.setCancelable(false);
        dialog.show();
    }

    public void disMiss(){
        dialog.dismiss();
    }

    public boolean isShowing(){
        return dialog.isShowing();
    }
}

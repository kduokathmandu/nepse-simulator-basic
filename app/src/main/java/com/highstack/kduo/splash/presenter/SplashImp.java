package com.highstack.kduo.splash.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.highstack.kduo.api.ApiInterface;
import com.highstack.kduo.helper.NoInternetConnectionHelper;
import com.highstack.kduo.splash.model.SplashParser;
import com.highstack.kduo.splash.view.SplashView;
import com.highstack.kduo.utils.Constant;
import com.highstack.kduo.utils.SetupRetrofit;


import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/17/2017.
 */

public class SplashImp implements SplashPresenter {

    SplashView splashView;
    Context context;

    public SplashImp(SplashView splashView, Context context) {
        this.splashView = splashView;
        this.context = context;
    }

    @Override
    public void getLiveData() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface liveDataInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = liveDataInterface.getLiveTrading();
        getCountry.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingtradingResponse", response.code() + "  " + response.raw().request().url());

                splashView.verifySucess();



            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }
}

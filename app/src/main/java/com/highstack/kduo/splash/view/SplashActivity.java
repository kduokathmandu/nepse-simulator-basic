package com.highstack.kduo.splash.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.highstack.kduo.MainActivity;
import com.highstack.kduo.R;
import com.highstack.kduo.base.BaseActivity;
import com.highstack.kduo.helper.SharedPreferencesHelper;
import com.highstack.kduo.livetrading.view.LiveTradingFragment;
import com.highstack.kduo.splash.presenter.SplashImp;
import com.highstack.kduo.splash.presenter.SplashPresenter;
import com.highstack.kduo.utils.Constant;

/**
 * Created by Own on 10/17/2017.
 */

public class SplashActivity extends BaseActivity implements SplashView{

    SharedPreferencesHelper sharedPreferencesHelper;
    SplashPresenter splashPresenter;

    @Override
    protected int getLayout() {
        return R.layout.splash_screen;
    }

    @Override
    protected void init() {
        splashPresenter = new SplashImp(SplashActivity.this,SplashActivity.this);
        splashPresenter.getLiveData();
    }


    @Override
    public void verifySucess() {
        LiveTradingFragment fragment = new LiveTradingFragment();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }






    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            Log.d("sdfdsfdsfdsf", "granted");
        }
    }
}
